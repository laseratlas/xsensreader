/*	Copyright (c) 2016 Laser Atlas All rights reserved.
*/

#include <xsens/xsportinfoarray.h>
#include <xsens/xsdatapacket.h>
#include <xsens/xstime.h>
#include <xcommunication/legacydatapacket.h>
#include <xcommunication/int_xsdatapacket.h>
#include <xcommunication/enumerateusbdevices.h>

#include "deviceclass.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <stdexcept>
#include <string>

#include <ctime>

#ifdef __GNUC__
#include "conio.h" // for non ANSI _kbhit() and _getch()
#else
#include <conio.h>
#endif

#include "logger.h"
#include "laComBusDriver.h"
#include "taskController.h"
#include "fileOutput.h"

namespace xsens {
	std::string port_name = "/dev/ttyUSB0";
	int baud_rate = 115200;
}

int main(int argc, char* argv[])
{
	DeviceClass device;
	std::ostringstream os;
	
	TaskController taskContrl;
	LaComBusDriver::setMessageParser(&taskContrl);
	taskContrl.setDataBus(LaComBusDriver::getInstance());
	LaComBusDriver::startRxTask();

	// Add logger output
	FileOutput fOut(LA_XSENS_LOG_FILE);
	Logger::addOutputs(&fOut);
	Logger::addOutputs(LaComBusDriver::getInstance());
	// Start logger thread
	Logger::start();
	
	try
	{
		XsPortInfoArray portInfoArray;
		xsEnumerateUsbDevices(portInfoArray);
		if (!portInfoArray.size())
		{
			std::string portName;
			int baudRate;
			XsPortInfo portInfo;
			
			if(argc == 1)
				portInfo.setPortName(xsens::port_name);
			else
				portInfo.setPortName(std::string(argv[1]));
				
			portInfo.setBaudrate(XsBaud::numericToRate(xsens::baud_rate));
			
			portInfoArray.push_back(portInfo);
		}

		// Use the first detected device
		XsPortInfo mtPort = portInfoArray.at(0);

		// Open the port with the detected device
		Logger::log("Opening port...", Logger::INFO);
		
		if (!device.openPort(mtPort))
			throw std::runtime_error("Could not open port. Aborting.");

		// Put the device in configuration mode
		Logger::log("Putting device into configuration mode...", Logger::INFO);
		if (!device.gotoConfig()) // Put the device into configuration mode before configuring the device
		{
			throw std::runtime_error("Could not put device into configuration mode. Aborting.");
		}

		// Request the device Id to check the device type
		mtPort.setDeviceId(device.getDeviceId());

		// Check if we have an MTi / MTx / MTmk4 device
		if (!mtPort.deviceId().isMt9c() && !mtPort.deviceId().isLegacyMtig() && !mtPort.deviceId().isMtMk4() && !mtPort.deviceId().isFmt_X000())
		{
			throw std::runtime_error("No MTi / MTx / MTmk4 device found. Aborting.");
		}
		
		os << "Found a device with id: " << mtPort.deviceId().toString().toStdString() << " @ port: " << mtPort.portName().toStdString() << ", baudrate: " << mtPort.baudrate();
	    Logger::log(os.str(), Logger::INFO);
	    os.str("");
	
		try
		{
			// Print information about detected device
			os << "Device: " << device.getProductCode().toStdString() << " opened.";
			Logger::log(os.str(), Logger::INFO);
			os.str("");
			
			// Configure the device
			Logger::log("Configuring the device...", Logger::INFO);
			if (mtPort.deviceId().isMt9c() || mtPort.deviceId().isLegacyMtig())
			{
				XsOutputMode outputMode = XOM_Orientation; // output orientation data
				XsOutputSettings outputSettings = XOS_OrientationMode_Quaternion; // output orientation data as quaternion

				// set the device configuration
				if (!device.setDeviceMode(outputMode, outputSettings))
				{
					throw std::runtime_error("Could not configure MT device. Aborting.");
				}
			}
			else if (mtPort.deviceId().isMtMk4() || mtPort.deviceId().isFmt_X000())
			{
				XsOutputConfiguration quat(XDI_Quaternion, 100);
				XsOutputConfigurationArray configArray;
				configArray.push_back(quat);
				if (!device.setOutputConfiguration(configArray))
				{

					throw std::runtime_error("Could not configure MTmk4 device. Aborting.");
				}
			}
			else
			{
				throw std::runtime_error("Unknown device while configuring. Aborting.");
			}
			
			// Change controller status to "READY TO START"
			taskContrl.setStatus(LA_DEVICE_STATUS_READY_TO_RUN);
			Logger::log("XSens is ready to start", Logger::INFO);
			// Waiting for the start measure CMD
			taskContrl.waitForStartCmd();
			// Put the device in measurement mode
			Logger::log("Putting device into measurement mode...", Logger::INFO);
			
			if (!device.gotoMeasurement())
			{
				throw std::runtime_error("Could not put device into measurement mode. Aborting.");
			}
			Logger::log("Running", Logger::INFO);
			
			XsByteArray data;
			XsMessageArray msgs;
			while (taskContrl.getStatus() != LA_DEVICE_STATUS_STOPPED)
			{
				device.readDataToBuffer(data);
				device.processBufferedData(data, msgs);
				for (XsMessageArray::iterator it = msgs.begin(); it != msgs.end(); ++it)
				{
					// Retrieve a packet
					XsDataPacket packet;
					if ((*it).getMessageId() == XMID_MtData) {
						LegacyDataPacket lpacket(1, false);
						lpacket.setMessage((*it));
						lpacket.setXbusSystem(false);
						lpacket.setDeviceId(mtPort.deviceId(), 0);
						lpacket.setDataFormat(XOM_Orientation, XOS_OrientationMode_Quaternion,0);	//lint !e534
						XsDataPacket_assignFromLegacyDataPacket(&packet, &lpacket, 0);
					}
					else if ((*it).getMessageId() == XMID_MtData2) {
						packet.setMessage((*it));
						packet.setDeviceId(mtPort.deviceId());
					}

					// Get the quaternion data
					XsQuaternion quaternion = packet.orientationQuaternion();
					os << "\r"
							  << "Time:" << std::time(nullptr) << " "
							  << "W:" << std::setw(5) << std::fixed << std::setprecision(2) << quaternion.w()
							  << ",X:" << std::setw(5) << std::fixed << std::setprecision(2) << quaternion.x()
							  << ",Y:" << std::setw(5) << std::fixed << std::setprecision(2) << quaternion.y()
							  << ",Z:" << std::setw(5) << std::fixed << std::setprecision(2) << quaternion.z()
					;

					// Convert packet to euler
					XsEuler euler = packet.orientationEuler();
					os << ",Roll:" << std::setw(7) << std::fixed << std::setprecision(2) << euler.roll()
							  << ",Pitch:" << std::setw(7) << std::fixed << std::setprecision(2) << euler.pitch()
							  << ",Yaw:" << std::setw(7) << std::fixed << std::setprecision(2) << euler.yaw()
					;
					taskContrl.addNewSensorDataFrame(os.str());
					os.str("");

				}
				msgs.clear();
				XsTime::msleep(0);
			}
		}
		catch (std::runtime_error const & error)
		{
			Logger::log(error.what(), Logger::ERROR);
		}
		catch (...)
		{
			Logger::log("An unknown fatal error has occured. Aborting.", Logger::ERROR);
		}

		// Close port
		Logger::log("Closing port...", Logger::INFO);
		device.close();
	}
	catch (std::runtime_error const & error)
	{
		Logger::log(error.what(), Logger::ERROR);
	}
	catch (...)
	{
		Logger::log("An unknown fatal error has occured. Aborting.", Logger::ERROR);
	}

	Logger::log("exit.", Logger::INFO);

	LaComBusDriver::stopRxTask();

	Logger::stop();
	Logger::join();

	return 0;
}
