/*
 * TaskController.h
 *
 *  Created on: Oct 27, 2016
 *      Author: Teng Wu
 */

#ifndef TASKCONTROLLER_H_
#define TASKCONTROLLER_H_

#include <mutex>
#include <condition_variable>
#include <fstream>
#include "laserAtlasConfig.h"
#include "laComBusMessageParser.h"
#include "laComBusDriver.h"

class TaskController : public LAComBusMessageParser {
public:
	TaskController();
	virtual ~TaskController();

	LaserAtlasDeviceStatus getStatus();
	void setStatus(LaserAtlasDeviceStatus status);

	const LaComBusDriver* getDataBus() const;
	void setDataBus(const LaComBusDriver* dataBus);

	void waitForStartCmd();
	void parse(const std::string & message);
	void addNewSensorDataFrame(const std::string & frame);

private:
	void sendStatusMessage();

	const LaComBusDriver * dataBus;
	LaserAtlasDeviceStatus status;
	std::ofstream dataSaveFile;
	std::mutex bus_mutex;
	std::condition_variable condition;
};
#endif /* TASKCONTROLLER_H_ */
