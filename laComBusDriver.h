/*
 * laComBusDriver.h
 *
 *  Created on: Oct 27, 2016
 *      Author: Teng Wu
 */

#ifndef LACOMBUSDRIVER_H_
#define LACOMBUSDRIVER_H_

#include "output.h"
#include <thread>
#include <mutex>
#include "laserAtlasConfig.h"
#include "laComBusMessageParser.h"

class LaComBusDriver: public Output {
public:
	static LaComBusDriver* getInstance();
	virtual ~LaComBusDriver();
	static void startRxTask();
	static void stopRxTask();
	void sendTxMessage(const std::string & msg) const;

	void write(const std::string & msg) const;
	std::string createMessage(LaserAtlasMessageType type, uint8_t subType, const std::string& content) const;
	bool extractInfoFromMessage(LaserAtlasMessageHeader *info, const std::string& message) const;
	static void setMessageParser(LAComBusMessageParser *parser);
private:
	LaComBusDriver();
	static void run();
	static bool isRxTaskRunning();
	static void setRxTaskRunningStatus(bool status);

	static LaComBusDriver* driver;
	static std::thread rxtask;
	static int bus_identifier;
	static bool rxTaskRunningStatus;
	static LAComBusMessageParser * parser;
	static std::mutex rx_mutex;
};

#endif /* LACOMBUSDRIVER_H_ */
